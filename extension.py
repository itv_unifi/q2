from q2_sdk.core.http_handlers.tecton_server_handler import Q2TectonServerRequestHandler
from q2_sdk.hq.models.db_config.db_config import DbConfig
from q2_sdk.hq.models.db_config.db_config_list import DbConfigList
from .install.db_plan import DbPlan
import re
import json
# import requests
import base64
import hmac
import hashlib
from q2_sdk.core import q2_requests
import asyncio

patternPhoneNumber = r'\+?([0-9]{11,})'
KEY_MESSAGE = {
    'THIRD_PARTY_ID_EXIST': "THIRD_PARTY_ID_EXIST",
    'PHONE_NUMBER_EXIST': "PHONE_NUMBER_EXIST",
    'PHONE_NUMBER_NOT_EXIST': "PHONE_NUMBER_NOT_EXIST"
}
NOTIFICATION_MESSAGE = {
    'PHONE_NUMBER_BLANK': 'Your mobile phone is currently blank. Please enter ten digits in the (123) 456-7890 format.',
    'PHONE_NUMBER_INVALID': "The phone number you are using \"{0}\" doesnt appear to be a valid US phone number. Please try again with ten digits in the (123) 456-7890 format.",
    'PHONE_NUMBER_EXIST': 'The phone number you are using "{0}" already existed in our system. Please try again with ten digits in the (123) 456-7890 format.',
    'CONFIRM_PHONE_NUMBER_ONBOARD_WITH_UNIFIMONEY': 'Your current phone number "{0}" will be used to onboard with us. Do you want to update this phone number?'
}

class UnifimoneySSOHandler(Q2TectonServerRequestHandler):

    WEDGE_ADDRESS_CONFIGS = DbConfigList(
        [
            DbConfig('SECRET_KEY', 'unifi_q2_@!'),
            DbConfig('ENVIROMENT_KEY', 'sandbox'),
            DbConfig('WEB_APP_UNIFI_URL', 'https://platform-qa.unifimoney.net'),
            DbConfig('API_UNIFI_URL', 'https://ni490yspn8.execute-api.us-west-1.amazonaws.com/unifi-money'),
        ]
    )

    # Set this to True if you want to change which Core is configured based on database configuration
    DYNAMIC_CORE_SELECTION = False

    # Set this True if you want your extension to be available to unauthenticated users
    # IS_UNAUTHENTICATED = False

    DB_PLAN = DbPlan()

    FRIENDLY_NAME = 'UnifimoneySSO'  # this will be used for end user facing references to this extension like Central and Menu items.
    DEFAULT_MENU_ICON = 'landing-page'  # this will be the default icon used if extension placed at top level (not used if a child element)

    CONFIG_FILE_NAME = 'UnifimoneySSO'  # configuration/UnifimoneySSO.py file must exist if REQUIRED_CONFIGURATIONS exist

    TECTON_URL = 'https://cdn1.onlineaccess1.com/cdn/base/tecton/v1.8.1/q2-tecton-sdk.js'

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        # self.variable_example = 12345

    @property
    def router(self):
        router = super().router
        router.update({
            'default': self.default,
            'submit': self.submit,
            'pre-onboard': self.pre_onboard,
            'interstital': self.interstital_introduce,
            'investment': self.investment_introduce
        })
        return router

    def get(self, *args, **kwargs):  # pylint: disable=unused-argument
        self.write("Hello World GET: From UnifimoneySSO")

    async def investment_introduce(self):
        template = self.get_template('investment.html.jinja2',{})
        html = self.get_tecton_form(
            "",
            custom_template=template,
            routing_key="interstital",
            hide_submit_button=True
        )
        return html
    
    async def interstital_introduce(self):
        template = self.get_template('interstitial.html.jinja2',{})
        html = self.get_tecton_form(
            "",
            custom_template=template,
            routing_key="pre-onboard",
            hide_submit_button=True
        )
        return html

    async def pre_onboard(self):
        template = self.get_template('reinput.html.jinja2', { 'notification_message': NOTIFICATION_MESSAGE['PHONE_NUMBER_BLANK']})
        html = self.get_tecton_form(
            "",
            custom_template=template,
            routing_key="submit",
            hide_submit_button=True
        )
        return html

    async def default(self):
        try:
            online_user = self.online_user;
            self.logger.info("online_user: %s", online_user.__dict__)
            checking_account_list = []
            if(len(self.account_list) > 0):
                for account in self.account_list:
                    if(account.hydra_product_code == 'C' or account.hydra_product_code == 'S'):
                        account_info = {
                            'account_aba': account.aba,
                            'account_number': account.acct_number_external_unmasked,
                            'account_type': account.hydra_product_code,
                            'host_acct_id': account._host_acct_id
                        }
                        checking_account_list.append(account_info)

            self.logger.info("checking_account_list: %s", checking_account_list)
            del online_user.demographic_info

            data = {
                'userId': online_user.user_id,
                'phoneNumber': online_user.mobile_phone,
                'first_name': online_user.first_name,
                'last_name': online_user.last_name,
                'email': online_user.email_address,
                'routing_number': self.hq_credentials.aba,
                'bankId': self.hq_credentials.aba,
                'checking_account_list': checking_account_list
            }
            payload = {**online_user.__dict__, **data}
            return await self.authorzationWithUnifimoney(payload)

        except Exception as error:
            self.logger.debug("[Error] default router: %s", error)
            template = self.get_template('submit.html.jinja2', {
                'message': error
            })

            html = self.get_tecton_form(
                "",
                custom_template=template,
                hide_submit_button=True
            )
            return html

    async def submit(self):
        try:
            newPhoneNumber = self.form_fields['region_code'] + self.form_fields['phone_number']
            if(not bool(newPhoneNumber) or not bool(re.match(patternPhoneNumber, newPhoneNumber))):
                # Re-input phone_number
                template = self.get_template('reinput.html.jinja2',{'notification_message': NOTIFICATION_MESSAGE['PHONE_NUMBER_INVALID'].format(newPhoneNumber)})
                html = self.get_tecton_form(
                    "",
                    custom_template=template,
                    routing_key="submit",
                    hide_submit_button=True
                )
                return html

            checking_account_list = []
            if(len(self.account_list) > 0):
                for account in self.account_list:
                    if(account.hydra_product_code == 'C' or account.hydra_product_code == 'S'):
                        account_info = {
                            'account_aba': account.aba,
                            'account_number': account.acct_number_external_unmasked,
                            'account_type': account.hydra_product_code,
                            'host_acct_id': account._host_acct_id
                        }
                        checking_account_list.append(account_info)

            self.logger.info("checking_account_list: %s", checking_account_list)

            online_user = self.online_user;
            del online_user.demographic_info
            data = {
                'userId': online_user.user_id,
                "phoneNumber": newPhoneNumber,
                'first_name': online_user.first_name,
                'last_name': online_user.last_name,
                'email': online_user.email_address,
                'routing_number': self.hq_credentials.aba,
                'bankId': self.hq_credentials.aba,
                'checking_account_list': checking_account_list
            }
            payload = {**online_user.__dict__, **data}
            self.logger.info("prepare payload send to Unifimoney: %s", payload)
            return await self.authorzationWithUnifimoney(payload, False)
        except Exception as err:
            self.logger.debug("[Error] submit router: %s", err)
            template = self.get_template('submit.html.jinja2', {
                'message': err
            })

            html = self.get_tecton_form(
                "",
                custom_template=template,
                hide_submit_button=True
            )
            return html

    async def authorzationWithUnifimoney(self, data, isFirstSubmit = True):
        WEB_APP_UNIFI_URL = 'https://platform-dev-2.unifimoney.net';
        # WEB_APP_UNIFI_URL = 'https://platform-qa.unifimoney.net';
        # WEB_APP_UNIFI_URL = 'https://platform-demo.unifimoney.net';
        # WEB_APP_UNIFI_URL = 'https://dvt7puw9fwq7g.cloudfront.net';
        API_UNIFI_URL = self.wedge_address_configs['API_UNIFI_URL']
        # API_UNIFI_URL = "https://ni490yspn8.execute-api.us-west-1.amazonaws.com/unifi-money"
        #//* Request to Back-office get code
        payloadHashed = self.generateQ2HeadersData(data);
        headers = {
            'Content-type': 'application/json',
            'X-Q2-PAYLOAD': payloadHashed['X-Q2-PAYLOAD'],
            'X-Q2-SIGNATURE': payloadHashed['X-Q2-SIGNATURE'],
            'Request_uuid': self.logger.extra.get('guid','')
        }
        self.logger.info("WEB_APP_UNIFI_URL: %s", WEB_APP_UNIFI_URL)
        self.logger.info("API_UNIFI_URL: %s", API_UNIFI_URL)
        self.logger.info("headers: %s", headers)

        result = await q2_requests.get(logger=self.logger, url = f'{API_UNIFI_URL}/v2/q2/check-exist', headers=headers, verify_whitelist=False);
        self.logger.info(f"{result.url}-{result.status_code}-{result.headers}-{result.reason}")
        self.logger.info(f'result >>> {result}')
        resultAsJson = result.json(); #//* To convert to Json format
        response = resultAsJson['data']
        self.logger.info(f'response >>> {response}')
        if(('key' not in response)):
            template = self.get_template('submit.html.jinja2', {
                'message': 'Authenticate with Unifimoney API failed'
            })

            html = self.get_tecton_form(
                "",
                custom_template=template,
                hide_submit_button=True
            )
            return html

        if(response['key'] == KEY_MESSAGE['PHONE_NUMBER_EXIST']):
            # //* Re-input phone_number
            template = self.get_template('reinput.html.jinja2',{ 'notification_message': NOTIFICATION_MESSAGE['PHONE_NUMBER_EXIST'].format(data.get('phoneNumber', ''))})
            html = self.get_tecton_form(
                "",
                custom_template=template,
                routing_key="submit",
                hide_submit_button=True
            )
            return html
        elif(response['key'] == KEY_MESSAGE['PHONE_NUMBER_NOT_EXIST']):
            # //* Redirect to onboard Unifimoney page
            authorizeCode = response["code"]
            if (isFirstSubmit):
                template = self.get_template('pre-onboard.html.jinja2',{
                    'url': f'{WEB_APP_UNIFI_URL}/sso?code={authorizeCode}',
                    'notification_message': NOTIFICATION_MESSAGE['CONFIRM_PHONE_NUMBER_ONBOARD_WITH_UNIFIMONEY'].format(data.get('phoneNumber', ''))
                })
                html = self.get_tecton_form(
                    "",
                    custom_template=template,
                    routing_key="submit",
                    hide_submit_button=True
                )
                return html
            else:
                template = self.get_template('redirect.html.jinja2',{
                    'url': f'{WEB_APP_UNIFI_URL}/sso?code={authorizeCode}'
                })
                html = self.get_tecton_form(
                    "",
                    custom_template=template,
                    hide_submit_button=True
                )
                return html
        elif(response['key'] == KEY_MESSAGE['THIRD_PARTY_ID_EXIST']):
            # //* Redirect to Unifimoney webapp
            authorizeCode = response["code"]
            template = self.get_template('popup.html.jinja2',{
                'url': f'{WEB_APP_UNIFI_URL}/sso?code={authorizeCode}'
            })
            html = self.get_tecton_form(
                "",
                custom_template=template,
                hide_submit_button=True
            )
            return html
        else:
            if(isFirstSubmit):
                return await self.investment_introduce()
            template = self.get_template('reinput.html.jinja2', { 'notification_message': NOTIFICATION_MESSAGE['PHONE_NUMBER_INVALID'].format(data.get('phoneNumber', ''))})
            if(not (data.get('phoneNumber', '') and data.get('phoneNumber', '').strip())):
                template = self.get_template('reinput.html.jinja2', { 'notification_message': NOTIFICATION_MESSAGE['PHONE_NUMBER_BLANK'].format(data.get('phoneNumber', ''))})
            html = self.get_tecton_form(
                "",
                custom_template=template,
                routing_key="submit",
                hide_submit_button=True
            )
            return html

    def generateQ2HeadersData(self, payload):
        # Convert payload to string
        payloadAsStr = json.dumps(payload)
        payloadInBytes = payloadAsStr.encode("ascii")
        # Convert payload to base64 bytes
        payloadInBase64Bytes = base64.b64encode(payloadInBytes)
        # Convert base64 to string
        payloadInBase64Str = payloadInBase64Bytes.decode()
        # self.logger.info("X-Q2-PAYLOAD >>> %s", payloadInBase64Str)
        # Create signature with secret key
        SECRET_KEY = self.wedge_address_configs['SECRET_KEY']
        signature = hmac.new(SECRET_KEY.encode(), payloadInBase64Bytes, hashlib.sha384).hexdigest()
        # self.logger.info(f"X-Q2-SIGNATURE >>> %s", signature)
        return {'X-Q2-PAYLOAD': payloadInBase64Str, 'X-Q2-SIGNATURE': signature}